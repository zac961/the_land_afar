# The_Land_Afar

Developed with Unreal Engine 4

Level design for Professional Portfolio

Taking inspiration from dungeon crawler games with the Top-Down camera view. It is meant to be a fantasy style game where the player roams around the world/level seeing all there is to see 
but at the same time, the player has to be on the lookout for the wandering mage that is out to destroy the player!